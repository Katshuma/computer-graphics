﻿# CS-C3100 Computer Graphics, Fall 2016
# Lehtinen / Granskog, Kemppinen, Ollikainen
#
# Assignment 3: Hierarchical transforms

Student name: Jussi "Katshuma" Hirvonen
Student number: 355496
Hours spent on requirements (approx.): 10
Hours spent on extra credit (approx.): 0

# First, a 10-second poll about this assignment period:

Did you go to exercise sessions?
No

Did you work on the assignment using Aalto computers, your own computers, or both?
Aalto Computers

# Which parts of the assignment did you complete? Mark them 'done'.
# You can also mark non-completed parts as 'attempted' if you spent a fair amount of
# effort on them. If you do, explain the work you did in the problems/bugs section
# and leave your 'attempt' code in place (commented out if necessary) so we can see it.

           R1 Calculating joint positions (1 p): done
                   R2 Rotating the joints (2 p): done
  R3 Visualizing joint coordinate systems (2 p): done
       R4 Skeletal subspace deformation (4 pts): done
                R5 Skinning for normals (1 pts): done

# Did you do any extra credit work?
(Describe what you did and, if there was a substantial amount of work involved, how you did it. Also describe how to use/activate your extra features, if they are interactive.)


# Are there any known problems/bugs remaining in your code?
(Please provide a list of the problems. If possible, describe what you think the cause is, how you have attempted to diagnose or fix the problem, and how you would attempt to diagnose or fix it if you had more time or motivation. This is important: we are more likely to assign partial credit if you help us understand what's going on.)
No apparent bugs.

# Did you collaborate with anyone in the class?
(Did you help others? Did others help you? Let us know who you talked to, and what sort of help you gave or received.)
I collaborated with Teo Raunio. We studied matrices together (though we still don't understand them as well as we'd like) and split the work in R3.

# Any other comments you'd like to share about the assignment or the course so far?
(Was the assignment too long? Too hard? Fun or boring? Did you learn something, or was it a total waste of time? Can we do something differently to help you learn? Please be brutally honest; we won't take it personally.)
This course is hard, but also awesome in its own way. Almost every hard to understand thing feels really smart once you understand its inner mechanics. It took us hours to figure out how the to-parent and to-world transformations work, but after the revelation all I could think was: "This is so clever! Who came up with this? Of course it should be done like this!" I really wish I could understand the matrices in general better though.
