﻿# ME-C3100 Computer Graphics, Fall 2016
# Lehtinen / Granskog, Kemppinen, Ollikainen
#
# Assignment 1: Introduction

Student name: Jussi "Katshuma" Hirvonen
Student number: 355496
Hours spent on requirements (approx.): 3.75
Hours spent on extra credit (approx.): 4

# First, some questions about where you come from and how you got started.
# Your answers in this section will be used to improve the course.
# They will not be judged or affect your points, but please answer all of them.
# Keep it short; answering shouldn't take you more than 5-10 minutes.

- What are you studying here at Aalto? (Department, major, minor...?)
Computer Science major, bachelor student.

- Which year of your studies is this?
4th.

- Is ME-C3100 a mandatory course for you?
No. It is one of the several courses from which I have to choose two optional ones, though.

- Have you had something to do with graphics before? Other studies, personal interests, work?
I've messed around with terrain generation before out of personal interest. I've spent a little bit of time generating visual representations of heightmaps.

- Do you remember basic linear algebra? Matrix and vector multiplication, cross product, that sort of thing?
All that has completely vanished from my mind after I passed the related courses. I'm going to have to relearn some things.

- How is your overall programming experience? What language are you most comfortable with?
I started programming for the first time in Aalto University. That amounts to 3 years worth of experience. The language I'm most comfortable with is most definitely Scala.

- Do you have some experience with these things? (If not, do you have experience with something similar such as C or Direct3D?)
C++: No experience. And from the initial look that I've had, I hate C++. I hope that impression changes.
C++11: I've seen this concept somewhere just recently but I have no idea how it differs from, say, C++10 or C++12.
OpenGL: I don't know what that is.

- Have you used a version control system such as Git, Mercurial or Subversion? Which ones?
I have used Git and am comfortable with it.
- Did you go to the technology lecture?
No.
- Did you go to exercise sessions?
No.
- Did you work on the assignment using Aalto computers, your own computers, or both?
Aalto computers.

# Which parts of the assignment did you complete? Mark them 'done'.
# You can also mark non-completed parts as 'attempted' if you spent a fair amount of
# effort on them. If you do, explain the work you did in the problems/bugs section
# and leave your 'attempt' code in place (commented out if necessary) so we can see it.
(Try to get everything done! Based on previous data, virtually everyone who put in the work and did well in the first two assignments ended up finishing the course, and also reported a high level of satisfaction at the end of the course.)

                            opened this file (0p): done
                         R1 Moving an object (1p): done
R2 Generating a simple cone mesh and normals (3p): done
  R3 Converting mesh data for OpenGL viewing (3p): done
           R4 Loading a large mesh from file (3p): done

# Did you do any extra credit work?
(Describe what you did and, if there was a substantial amount of work involved, how you did it. Also describe how to use/activate your extra features, if they are interactive.)

Version control
Camera mouse control (I tried to make a completely free camera but I just couldn't figure out how to combine the horizontal and vertical movement. The feature is lacking.)
Animation
Viewport correction

# Are there any known problems/bugs remaining in your code?
(Please provide a list of the problems. If possible, describe what you think the cause is, how you have attempted to diagnose or fix the problem, and how you would attempt to diagnose or fix it if you had more time or motivation. This is important: we are more likely to assign partial credit if you help us understand what's going on.)



# Did you collaborate with anyone in the class?
(Did you help others? Did others help you? Let us know who you talked to, and what sort of help you gave or received.)

I completed the required assignments together with Teo Raunio. We helped each other whenever we got stuck.

# Any other comments you'd like to share about the assignment or the course so far?
(Was the assignment too long? Too hard? Fun or boring? Did you learn something, or was it a total waste of time? Can we do something differently to help you learn? Please be brutally honest; we won't take it personally.)

I wish I would have a better understanding of all the code inside the solution. The whole framework thing still goes way over my head. But I did learn a lot, so I think the assignment was super helpful.
