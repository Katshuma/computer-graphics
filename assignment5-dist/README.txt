﻿# ME-C3100 Computer Graphics, Fall 2016
# Lehtinen / Kemppinen, Ollikainen, Granskog
#
# Assignment 5: Ray Tracing

Student name: Jussi "Katshuma" Hirvonen
Student number: 355496
Hours spent on requirements (approx.): 10
Hours spent on extra credit (approx.): 0

# First, a 10-second poll about this assignment period:

Did you go to exercise sessions?
No

Did you work on the assignment using Aalto computers, your own computers, or both?
Aalto Computers

# Which parts of the assignment did you complete? Mark them 'done'.
# You can also mark non-completed parts as 'attempted' if you spent a fair amount of
# effort on them. If you do, explain the work you did in the problems/bugs section
# and leave your 'attempt' code in place (commented out if necessary) so we can see it.

R1 Generating rays & ambient lighting (  1p): done
R2 Visualizing depth		      (0.5p): done
R3 Perspective camera		      (0.5p): done
R4 Phong shading		      (  3p): attempted (should be done, but the r4_point_light_circle doesn't work, did they require an extra task to be completed?)
R5 Plane intersection		      (0.5p): done
R6 Triangle intersection	      (  1p): done
R7 Shadows			      (  1p): done
R8 Mirror reflection		      (  1p): done
R9 Antialiasing			      (1.5p): not done

# Did you do any extra credit work?
(Describe what you did and, if there was a substantial amount of work involved, how you did it. Also describe how to use/activate your extra features, if they are interactive.)

No

# Are there any known problems/bugs remaining in your code?
(Please provide a list of the problems. If possible, describe what you think the cause is, how you have attempted to diagnose or fix the problem, and how you would attempt to diagnose or fix it if you had more time or motivation. This is important: we are more likely to assign partial credit if you help us understand what's going on.)

r4_point_light_circle doesn't work. I'm not really sure if it's even supposed to at this point. But I gave R4 my best shot.

# Did you collaborate with anyone in the class?
(Did you help others? Did others help you? Let us know who you talked to, and what sort of help you gave or received.)

I did collaborate with Teo Raunio. He helped me with R4 a lot.

# Any other comments you'd like to share about the assignment or the course so far?
(Was the assignment too long? Too hard? Fun or boring? Did you learn something, or was it a total waste of time? Can we do something differently to help you learn? Please be brutally honest; we won't take it personally.)

I really disliked how the r4_point_light_circle was (and this is only an educated guess) not even supposed to work before something else had been implemented. I wasted at least an hour trying to figure out what was wrong with my R4 solution, when I should've just moved on to the next task.