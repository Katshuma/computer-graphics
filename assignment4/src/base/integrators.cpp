
#include "utility.hpp"
#include "particle_systems.hpp"
#include "integrators.hpp"

void eulerStep(ParticleSystem& ps, float step) {
	// YOUR CODE HERE (R1)
	// Implement an Euler integrator.
	const auto &x0 = ps.state();
	auto n = x0.size();
	//for (auto y = 0; y < n; y++) {
	//	std::cout << x0[y][0] << " " << x0[y][1] << " " << x0[y][2] << std::endl;
	//}
	//std::cout << std::endl;
	auto f0 = ps.evalF(x0);
	auto nextStep = State(n);
	for (auto x = 0u; x < n; x++) {
		nextStep[x] = x0[x] + step * f0[x];
	}
	ps.set_state(nextStep);
};

void trapezoidStep(ParticleSystem& ps, float step) {
	// YOUR CODE HERE (R3)
	// Implement a trapezoid integrator.
	const auto &x0 = ps.state();
	auto n = x0.size();
	//for (auto y = 0; y < n; y++) {
	//	std::cout << x0[y][0] << " " << x0[y][1] << " " << x0[y][2] << std::endl;
	//}
	//std::cout << std::endl;
	auto f0 = ps.evalF(x0);
	auto nextStep = State(n);
	auto finalStep = State(n);
	for (auto x = 0u; x < n; x++) {
		nextStep[x] = x0[x] + step * f0[x];
	}
	auto f1 = ps.evalF(nextStep);
	for (auto y = 0u; y < n; y++) {
		finalStep[y] = x0[y] + (step * 0.5f) * (f0[y] + f1[y]);
	}
	ps.set_state(finalStep);
}

void midpointStep(ParticleSystem& ps, float step) {
	const auto& x0 = ps.state();
	auto n = x0.size();
	auto f0 = ps.evalF(x0);
	auto xm = State(n), x1 = State(n);
	for (auto i = 0u; i < n; ++i) {
		xm[i] = x0[i] + (0.5f * step) * f0[i];
	}
	auto fm = ps.evalF(xm);
	for (auto i = 0u; i < n; ++i) {
		x1[i] = x0[i] + step * fm[i];
	}
	ps.set_state(x1);
}

void rk4Step(ParticleSystem& ps, float step) {
	// EXTRA: Implement the RK4 Runge-Kutta integrator.
	const auto& x0 = ps.state();
	auto n = x0.size();
	auto f0 = ps.evalF(x0);
	auto x2 = State(n), x3 = State(n), x4 = State(n), k1 = State(n), k2 = State(n), k3 = State(n), k4 = State(n), kn = State(n);
	for (auto i = 0u; i < n; ++i) {
		k1[i] = step * f0[i];
	}
	for (auto i = 0u; i < n; ++i) {
		x2[i] = x0[i] + (k1[i] / 2.f);
	}
	auto f2 = ps.evalF(x2);
	for (auto i = 0u; i < n; ++i) {
		k2[i] = step * f2[i];
	}
	for (auto i = 0u; i < n; ++i) {
		x3[i] = x0[i] + (k2[i] / 2.f);
	}
	auto f3 = ps.evalF(x3);
	for (auto i = 0u; i < n; ++i) {
		k3[i] = step * f3[i];
	}
	for (auto i = 0u; i < n; ++i) {
		x4[i] = x0[i] + k3[i];
	}
	auto f4 = ps.evalF(x4);
	for (auto i = 0u; i < n; ++i) {
		k4[i] = step * f4[i];
	}
	for (auto i = 0u; i < n; ++i) {
		kn[i] = x0[i] + (1.f / 6.f) * k1[i] + (1.f / 3.f) * k2[i] + (1.f / 3.f) * k3[i] + (1.f / 6.f) * k4[i];
	}
	ps.set_state(kn);
}

#ifdef EIGEN_SPARSECORE_MODULE_H

void implicit_euler_step(ParticleSystem& ps, float step, SparseMatrix& J, SparseLU& solver, bool initial) {
	// EXTRA: Implement the implicit Euler integrator. (Note that the related formula on page 134 on the lecture slides is missing a 'h'; the formula should be (I-h*Jf(Yi))DY=-F(Yi))
}

void implicit_midpoint_step(ParticleSystem& ps, float step, SparseMatrix& J, SparseLU& solver, bool initial) {
	// EXTRA: Implement the implicit midpoint integrator.
}
#endif
