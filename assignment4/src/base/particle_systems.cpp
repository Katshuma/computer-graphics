#include "particle_systems.hpp"

#include <algorithm>
#include <cassert>
#include <iostream>
#include <numeric>

using namespace std;
using namespace FW;

namespace {

	inline Vec3f fGravity(float mass) {
		return Vec3f(0, -9.8f * mass, 0);
	}

	inline Vec3f fSpring(const Vec3f& pos1, const Vec3f& pos2, float k, float rest_length) {
		Vec3f d = pos1 - pos2;
		float norm = sqrtf(d[0] * d[0] + d[1] * d[1] + d[2] * d[2]);
		return Vec3f(-k * (norm - rest_length) * (d[0] / norm), -k * (norm - rest_length) * (d[1] / norm), -k * (norm - rest_length) * (d[2] / norm));
	}

	inline Vec3f fDrag(const Vec3f& v, float k) {
		return Vec3f(-k * v[0], -k * v[1], -k * v[2]);
	}

} // namespace

void SimpleSystem::reset() {
	state_ = State(1, Vec3f(0, radius_, 0));
}

State SimpleSystem::evalF(const State& state) const {
	State f(1, Vec3f(-state[0].y, state[0].x, 0));
	return f;
}

#ifdef EIGEN_SPARSECORE_MODULE_H
// using the implicit Euler method, the simple system should converge towards origin -- as opposed to the explicit Euler, which diverges outwards from the origin.
void SimpleSystem::evalJ(const State&, SparseMatrix& result, bool initial) const {
	if (initial) {
		result.coeffRef(1, 0) = 1.0f;
		result.coeffRef(0, 1) = -1.0f;
	}
}
#endif

Points SimpleSystem::getPoints() {
	return Points(1, state_[0]);
}

Lines SimpleSystem::getLines() {
	static const auto n_lines = 50u;
	auto l = Lines(n_lines * 2);
	const auto angle_incr = 2 * FW_PI / n_lines;
	for (auto i = 0u; i < n_lines; ++i) {
		l[2 * i] = l[2 * i + 1] =
			Vec3f(radius_ * FW::sin(angle_incr * i), radius_ * FW::cos(angle_incr * i), 0);
	}
	rotate(l.begin(), l.begin() + 1, l.end());
	return l;
}

void SpringSystem::reset() {
	const auto start_pos = Vec3f(0.1f, -0.5f, 0.0f);
	const auto spring_k = 30.0f;
	state_ = State(4);
	// YOUR CODE HERE (R2)
	// Set the initial state for a particle system with one particle fixed
	// at origin and another particle hanging off the first one with a spring.
	// Place the second particle initially at start_pos.
	spring_ = Spring(2, 0, spring_k, 0.5f);
	state_[0] = Vec3f(0.f, 0.f, 0.f);
	state_[1] = Vec3f(0.f, 0.f, 0.f);
	state_[2] = start_pos;
	state_[3] = Vec3f(0.f, 0.f, 0.f);
}

State SpringSystem::evalF(const State& state) const {
	const auto mass = 1.0f;
	const auto drag_k = 0.5f;
	State f(4);
	// YOUR CODE HERE (R2)
	// Return a derivative for the system as if it was in state "state".
	// You can use the fGravity, fDrag and fSpring helper functions for the forces.
	f[0] = Vec3f(0.f, 0.f, 0.f);
	f[1] = Vec3f(0.f, 0.f, 0.f);
	f[2] = state[3];
	f[3] = fGravity(mass) + fSpring(state[spring_.i1], state[spring_.i2], spring_.k, spring_.rlen) + fDrag(state[3], drag_k);
	return f;
}

#ifdef EIGEN_SPARSECORE_MODULE_H

// This is a very useful read for the Jacobians of the spring forces. It deals with spring damping as well, we don't do that -- our drag is simply a linear damping of velocity (that results in some constants in the Jacobian).
// http://blog.mmacklin.com/2012/05/04/implicitsprings/

void SpringSystem::evalJ(const State& state, SparseMatrix& result, bool initial) const {
	const auto drag_k = 0.5f;
	const auto mass = 1.0f;
	// EXTRA: Evaluate the Jacobian into the 'result' matrix here. Only the free end of the spring should have any nonzero values related to it.
}
#endif

Points SpringSystem::getPoints() {
	auto p = Points(2);
	p[0] = state_[0]; p[1] = state_[2];
	return p;
}

Lines SpringSystem::getLines() {
	auto l = Lines(2);
	l[0] = state_[0]; l[1] = state_[2];
	return l;
}

void PendulumSystem::reset() {
	const auto spring_k = 1000.0f;
	const auto start_point = Vec3f(0);
	const auto end_point = Vec3f(0.05, -1.5, 0);
	state_ = State(2 * n_);
	// YOUR CODE HERE (R4)
	// Set the initial state for a pendulum system with n_ particles
	// connected with springs into a chain from start_point to end_point.
	springs_.clear();
	state_[0] = start_point;
	state_[1] = Vec3f(0.f, 0.f, 0.f);
	for (auto x = 0u; x < n_; x++) {
		float slider = float(x) / (float(n_) - 1);
		if (x != 0) {
			state_[x * 2] = Vec3f(slider * end_point[0], slider * end_point[1], slider * end_point[2]);
			state_[(x * 2) + 1] = Vec3f(0.f, 0.f, 0.f);
			Vec3f d = state_[x * 2] - state_[(x * 2) - 2];
			springs_.push_back(Spring(x, x - 1, spring_k / n_, sqrtf(d[0] * d[0] + d[1] * d[1] + d[2] * d[2])));
		}
	}
}

State PendulumSystem::evalF(const State& state) const {
	const auto drag_k = 0.5f;
	const auto mass = 0.5f;
	auto f = State(2 * n_);
	// YOUR CODE HERE (R4)
	// As in R2, return a derivative of the system state "state".
	f[0] = Vec3f(0.f, 0.f, 0.f);
	f[1] = Vec3f(0.f, 0.f, 0.f);
	for (auto a = 2u; a < (2 * n_) - 1; a += 2) {
		f[a] = state[a + 1];
		f[a + 1] = f[a - 1] + fGravity(mass) + fSpring(state[springs_[(a / 2) - 1].i1 * 2], state[springs_[(a / 2) - 1].i2 * 2], springs_[(a / 2) - 1].k, springs_[(a / 2) - 1].rlen) + fDrag(state[a + 1], drag_k);
	}
	return f;
}

#ifdef EIGEN_SPARSECORE_MODULE_H

void PendulumSystem::evalJ(const State& state, SparseMatrix& result, bool initial) const {

	const auto drag_k = 0.5f;
	const auto mass = 0.5f;

	// EXTRA: Evaluate the Jacobian here. Each spring has an effect on four blocks of the matrix -- both of the positions of the endpoints will have an effect on both of the velocities of the endpoints.
}
#endif


Points PendulumSystem::getPoints() {
	auto p = Points(n_);
	for (auto i = 0u; i < n_; ++i) {
		p[i] = state_[i * 2];
	}
	return p;
}

Lines PendulumSystem::getLines() {
	auto l = Lines();
	for (const auto& s : springs_) {
		l.push_back(state_[2 * s.i1]);
		l.push_back(state_[2 * s.i2]);
	}
	return l;
}

void ClothSystem::reset() {
	const auto spring_k = 300.0f;
	const auto width = 1.5f, height = 1.5f; // width and height of the whole grid
	state_ = State(2 * x_ * y_);
	// YOUR CODE HERE (R5)
	// Construct a particle system with a x_ * y_ grid of particles,
	// connected with a variety of springs as described in the handout:
	// structural springs, shear springs and flex springs.
	springs_.clear();
	int i = 0;
	for (auto y = 0u; y < y_; y++) {
		for (auto x = 0u; x < x_; x++) {
			state_[i] = Vec3f(((width / x_) * x) - 0.75f, 0.f, (height / y_) * y);
			state_[i + 1] = Vec3f(0.f, 0.f, 0.f);

			// structural
			if (y == 0) {
				if (x != 0) {
					springs_.push_back(Spring(i / 2, (i / 2) - 1, spring_k, width / x_));
				}
			}
			else if (x == 0) {
				springs_.push_back(Spring(i / 2, (i / 2) - x_, spring_k, width / x_));
			}
			else {
				springs_.push_back(Spring(i / 2, (i / 2) - 1, spring_k, width / x_));
				springs_.push_back(Spring(i / 2, (i / 2) - x_, spring_k, width / x_));
			}

			// shear
			if (y != 0) {
				if (x % 2 == 0) {
					springs_.push_back(Spring(i / 2, ((i / 2) - x_) + 1, spring_k, sqrtf(((width / x_) * (width / x_)) * 2)));
				}
				else {
					springs_.push_back(Spring(i / 2, ((i / 2) - x_) - 1, spring_k, sqrtf(((width / x_) * (width / x_)) * 2)));
				}
			}

			// flex
			if (y == 0) {
				if (x != 0 && x != 1) {
					springs_.push_back(Spring(i / 2, (i / 2) - 2, spring_k, (width / x_) * 2));
				}
			}
			else if (x == 0) {
				if (y != 1) {
					springs_.push_back(Spring(i / 2, (i / 2) - (x_ * 2), spring_k, (width / x_) * 2));
				}
			}
			else {
				if (x != 1 && y != 1) {
					springs_.push_back(Spring(i / 2, (i / 2) - 2, spring_k, (width / x_) * 2));
					springs_.push_back(Spring(i / 2, (i / 2) - (x_ * 2), spring_k, (width / x_) * 2));
				}
			}

			i += 2;
		}
	}
}

State ClothSystem::evalF(const State& state) const {
	const auto drag_k = 0.08f;
	const auto n = x_ * y_;
	static const auto mass = 0.025f;
	auto f = State(2 * n);
	std::vector<Spring> temp;
	// YOUR CODE HERE (R5)
	// This will be much like in R2 and R4.
	for (auto a = 0u; a < (n * 2) - 1; a += 2) {
		f[a] = state[a + 1];
		f[a + 1] = fGravity(mass) + fDrag(state[a + 1], drag_k);
		temp.clear();
		for (auto x = 0; x < springs_.size(); x++) {
			if (springs_[x].i1 == a / 2 || springs_[x].i2 == a / 2) {
				temp.push_back(springs_[x]);
			}
		}
		for (auto y = 0; y < temp.size(); y++) {
			f[a + 1] += fSpring(state[temp[y].i1 * 2], state[temp[y].i2 * 2], temp[y].k, temp[y].rlen);
		}
	}
	f[1] = Vec3f(0.f, 0.f, 0.f);
	f[(x_ * 2) - 1] = Vec3f(0.f, 0.f, 0.f);
	return f;
}

#ifdef EIGEN_SPARSECORE_MODULE_H

void ClothSystem::evalJ(const State& state, SparseMatrix& result, bool initial) const {
	const auto drag_k = 0.08f;
	static const auto mass = 0.025f;

	// EXTRA: Evaluate the Jacobian here. The code is more or less the same as for the pendulum.
}

#endif

Points ClothSystem::getPoints() {
	auto n = x_ * y_;
	auto p = Points(n);
	for (auto i = 0u; i < n; ++i) {
		p[i] = state_[2 * i];
	}
	return p;
}

Lines ClothSystem::getLines() {
	auto l = Lines();
	for (const auto& s : springs_) {
		l.push_back(state_[2 * s.i1]);
		l.push_back(state_[2 * s.i2]);
	}
	return l;
}
State FluidSystem::evalF(const State&) const {
	return State();
}

