﻿# ME-C3100 Computer Graphics, Fall 2016
# Lehtinen / Kemppinen, Ollikainen, Granskog
#
# Assignment 4: Physical Simulation

Student name: Jussi "Katshuma" Hirvonen
Student number: 355496
Hours spent on requirements (approx.): 8
Hours spent on extra credit (approx.): 1

# First, a 10-second poll about this assignment period:

Did you go to exercise sessions?
No

Did you work on the assignment using Aalto computers, your own computers, or both?
Aalto Computers

# Which parts of the assignment did you complete? Mark them 'done'.
# You can also mark non-completed parts as 'attempted' if you spent a fair amount of
# effort on them. If you do, explain the work you did in the problems/bugs section
# and leave your 'attempt' code in place (commented out if necessary) so we can see it.

     R1 Euler integrator (1p): done
        R2 Spring system (2p): done
 R3 Trapezoid integrator (2p): done
      R4 Pendulum system (2p): done
         R5 Cloth system (3p): attempted

# Did you do any extra credit work?
(Describe what you did and, if there was a substantial amount of work involved, how you did it. Also describe how to use/activate your extra features, if they are interactive.)

RK4 Runge-Kutta integrator has been implemented.



# Are there any known problems/bugs remaining in your code?
(Please provide a list of the problems. If possible, describe what you think the cause is, how you have attempted to diagnose or fix the problem, and how you would attempt to diagnose or fix it if you had more time or motivation. This is important: we are more likely to assign partial credit if you help us understand what's going on.)

The springs in R4 Pendulum system do not take into consideration the spring below them. I figured this bug out too late to dedicate any time into fixing it.
I also ran into a wall with R5 Cloth system. I feel like I'm really close to a potential solution but just couldn't get it to work. Also, my code seems to be atrociously slow compared to the example solution.



# Did you collaborate with anyone in the class?
(Did you help others? Did others help you? Let us know who you talked to, and what sort of help you gave or received.)

I worked together with Teo Raunio and Antti Kurkinen through most of the exercise.



# Any other comments you'd like to share about the assignment or the course so far?
(Was the assignment too long? Too hard? Fun or boring? Did you learn something, or was it a total waste of time? Can we do something differently to help you learn? Please be brutally honest; we won't take it personally.)

I just can't wait for Ray Tracing. Looking forward to the next exercise round.