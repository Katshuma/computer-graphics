﻿# ME-C3100 Computer Graphics, Fall 2016
# Lehtinen / Kemppinen, Ollikainen, Granskog
#
# Assignment 6: Real-Time Shading

Student name: Jussi "Katshuma" Hirvonen
Student number: 355496
Hours spent on requirements (approx.): 6
Hours spent on extra credit (approx.): 0

# First, a 10-second poll about this assignment period:

Did you go to exercise sessions?
No

Did you work on the assignment using Aalto computers, your own computers, or both?
Aalto computers

# Which parts of the assignment did you complete? Mark them 'done'.
# You can also mark non-completed parts as 'attempted' if you spent a fair amount of
# effort on them. If you do, explain the work you did in the problems/bugs section
# and leave your 'attempt' code in place (commented out if necessary) so we can see it.

R1       Sample diffuse texture (1p): done
R2        Sample normal texture (1p): done
R3  Blinn-Phong diffuse shading (2p): done
R4 Blinn-Phong specular shading (2p): attempted
R5     Normal transform insight (4p): not done

# Did you do any extra credit work?
(Describe what you did and, if there was a substantial amount of work involved, how you did it. Also describe how to use/activate your extra features, if they are interactive.)

No.

# Are there any known problems/bugs remaining in your code?
(Please provide a list of the problems. If possible, describe what you think the cause is, how you have attempted to diagnose or fix the problem, and how you would attempt to diagnose or fix it if you had more time or motivation. This is important: we are more likely to assign partial credit if you help us understand what's going on.)

The solution for R4 seems to be a bit off. Can't figure out the reason why.

# Did you collaborate with anyone in the class?
(Did you help others? Did others help you? Let us know who you talked to, and what sort of help you gave or received.)

Teo Raunio helped me. We tried solving R4 together but this is as close as we could get.

# Any other comments you'd like to share about the assignment or the course so far?
(Was the assignment too long? Too hard? Fun or boring? Did you learn something, or was it a total waste of time? Can we do something differently to help you learn? Please be brutally honest; we won't take it personally.)

I wish turning in extra credit work even after the Assignment 6 deadline has passed was allowed.